﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Api.Exceptions;
using Api.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VyVApi.BusinessLogic.Abstractions;
using VyVApi.BusinessLogic.Dtos;

namespace VyVApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HorariosController : Controller
    {
        private readonly IHorario horario;

        public HorariosController(IHorario horario)
        {
            this.horario = horario;
        }

        [HttpGet]
        [Route("{materiaId:int}")]
        public IActionResult Get(int materiaId)
        {
            try
            {
                var materias = horario.GetHorarioDtosByMateriaId(materiaId);
                if (materias.Count() == 0) throw new ValidateException("No Existen horarios para esta materia.");
                return Ok(materias);
            }
            catch (ValidateException ex)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, ex.GetInnerMessages());
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.GetInnerMessages());
            }
        }

        [HttpPost]
        public IActionResult Post([FromBody] HorarioDto dto)
        {
            try
            {
                horario.InscribirAlumno(dto);
                return Ok(dto);
            }
            catch (ValidateException ex)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, ex.GetInnerMessages());
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.GetInnerMessages());
            }
        }
    }
}