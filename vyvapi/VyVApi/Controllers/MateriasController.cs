﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Api.Exceptions;
using Api.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using VyVApi.BusinessLogic.Abstractions;

namespace VyVApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MateriasController : ControllerBase
    {
        private readonly IMateria materia;

        public MateriasController(IMateria materia)
        {
            this.materia = materia;
        }

        [HttpGet]
        
        public IActionResult Get()
        {
            try
            {
                var materias = materia.GetMateriaDtos();
                return Ok(materias);
            }
            catch (ValidateException ex)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, ex.GetInnerMessages());
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.GetInnerMessages());
            }
        }

        [HttpGet]
        [Route("{id:int}")]
        public IActionResult Get(int id)
        {
            try
            {
                var materias = materia.GetMateriaDtos().FirstOrDefault(t=> t.Id ==id);
                if(materias == null) throw new ValidateException("No existe está materia.");
                return Ok(materias);
            }
            catch (ValidateException ex)
            {
                return StatusCode((int)HttpStatusCode.BadRequest, ex.GetInnerMessages());
            }
            catch (Exception ex)
            {
                return StatusCode((int)HttpStatusCode.InternalServerError, ex.GetInnerMessages());
            }
        }
    }
}