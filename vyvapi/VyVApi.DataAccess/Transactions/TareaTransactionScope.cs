﻿using Api.Attributes;
using Api.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using VyVApi.DataAccess.Abstranctions;

namespace VyVApi.DataAccess.Transactions
{
    [ApiRepository(LifeTime = LifeTime.Transient)]
    public class TareaTransactionScope<TContext> : IApiTransactionScope<TContext> where TContext : DbContext
    {
        private readonly TContext context;

        public TareaTransactionScope(TContext context)
        {
            this.context = context;
        }
        public IApiTransaction InitTransaction()
        {
            var current = context.Database.CurrentTransaction;
            if (current != null) return new ApiTransaction(null);

            var transaction = context.Database.BeginTransaction();
            return new ApiTransaction(transaction);
        }
    }
}