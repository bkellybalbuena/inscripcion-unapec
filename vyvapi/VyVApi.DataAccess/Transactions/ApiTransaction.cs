﻿using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.Text;
using VyVApi.DataAccess.Abstranctions;

namespace VyVApi.DataAccess.Transactions
{
    public class ApiTransaction : IApiTransaction
    {
        private IDbContextTransaction transaction;

        public ApiTransaction(IDbContextTransaction transaction)
        {
            this.transaction = transaction;
        }

        public void Commit()
        {
            transaction?.Commit(); this.transaction = null;
        }

        public void RollBack()
        {
            transaction?.Rollback();
        }

        public void Dispose()
        {
            RollBack();
        }

    }
}
