﻿using Api.Net.Core.DataAccess.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace VyVApi.DataAccess.Entities
{
   public class Horario : BaseEntity<Horario,TareaContext>
    {
        public int Id { get; set; }
        public int IdMateria { get; set; }
        public bool Estado { get; set; }
        public string Aula { get; set; }
        public string Grupo { get; set; }
        public string Lun { get; set; }
        public string Mar { get; set; }
        public string Mir { get; set; }
        public string Jue { get; set; }
        public string Vie { get; set; }
        public string Sab { get; set; }
        public string Dom { get; set; }
        public int Modulo { get; set; }
        public bool Seleccionado { get; set; }
        public int? Cantidad { get; set; }
        public int?Proximo { get; set; }

        public override void Configure(EntityTypeBuilder<Horario> builder)
        {
            builder.ToTable(nameof(Horario),"dbo");
        }
    }
}
