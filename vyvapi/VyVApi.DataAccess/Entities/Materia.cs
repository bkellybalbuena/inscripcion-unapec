﻿using Api.Net.Core.DataAccess.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;

namespace VyVApi.DataAccess.Entities
{
    public class Materia : BaseEntity<Materia, TareaContext>
    {
        public int Id { get; set; }
        public int IdCuatrimestre { get; set; }
        public bool Estado { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int Creditos { get; set; }
        public bool Seleccionado { get; set; }
        public override void Configure(EntityTypeBuilder<Materia> builder)
        {
            builder.ToTable(nameof(Materia), "dbo");
        }
    }
}
