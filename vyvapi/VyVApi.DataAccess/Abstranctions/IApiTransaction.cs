﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VyVApi.DataAccess.Abstranctions
{
    public interface IApiTransaction : IDisposable
    {
        void Commit();
        void RollBack();
    }
}
