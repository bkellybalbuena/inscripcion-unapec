﻿using Api.Attributes;
using Api.DataAcess.Context;
using Api.Enums;
using Microsoft.EntityFrameworkCore;
using System;

namespace VyVApi.DataAccess
{
    [ApiContext(LifeTime = LifeTime.Scoped)]
    public class TareaContext : BaseDbContext
    {
        public TareaContext(DbContextOptions options) : base(options)
        {
        }
    }
}
