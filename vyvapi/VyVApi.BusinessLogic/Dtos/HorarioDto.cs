﻿using Api.Dto.Base;
using System;
using System.Collections.Generic;
using System.Text;
using VyVApi.DataAccess.Entities;

namespace VyVApi.BusinessLogic.Dtos
{
    public class HorarioDto : Dto<HorarioDto,Horario>
    {
        public int Id { get; set; }
        public int IdMateria { get; set; }
        public bool Estado { get; set; }
        public string Aula { get; set; }
        public string Grupo { get; set; }
        public string Lun { get; set; }
        public string Mar { get; set; }
        public string Mir { get; set; }
        public string Jue { get; set; }
        public string Vie { get; set; }
        public string Sab { get; set; }
        public string Dom { get; set; }
        public int Modulo { get; set; }
        public bool Seleccionado { get; set; }
        public int Cantidad { get; set; }
        public int Proximo { get; set; }
    }
}
