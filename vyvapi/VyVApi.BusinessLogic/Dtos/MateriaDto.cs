﻿using Api.Attributes;
using Api.Dto.Base;
using System;
using System.Collections.Generic;
using System.Text;
using VyVApi.DataAccess.Entities;

namespace VyVApi.BusinessLogic.Dtos
{
    
    public class MateriaDto : Dto<MateriaDto,Materia>
    {
        public int Id { get; set; }
        public int IdCuatrimestre { get; set; }
        public bool Estado { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }
        public int Creditos { get; set; }
        public bool Seleccionado { get; set; }
    }
}
