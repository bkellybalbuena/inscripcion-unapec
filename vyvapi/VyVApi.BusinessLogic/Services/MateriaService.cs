﻿using Api.Attributes;
using Api.Repositories;
using Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VyVApi.BusinessLogic.Abstractions;
using VyVApi.BusinessLogic.Dtos;
using VyVApi.DataAccess.Entities;

namespace VyVApi.BusinessLogic.Services
{
    [ApiService]
    public class MateriaService : Service<MateriaDto, Materia>, IMateria
    {
        public MateriaService(IRepository<Materia> repository) : base(repository)
        {
        }

        public IEnumerable<MateriaDto> GetMateriaDtos()
        {
            return GetDto().ToList(); 
        }
    }
}
