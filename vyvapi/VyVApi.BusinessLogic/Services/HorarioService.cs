﻿using Api.Attributes;
using Api.Models;
using Api.Repositories;
using Api.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using VyVApi.BusinessLogic.Abstractions;
using VyVApi.BusinessLogic.Dtos;
using VyVApi.DataAccess.Entities;

namespace VyVApi.BusinessLogic.Services
{
    [ApiService]
    public class HorarioService : Service<HorarioDto, Horario>, IHorario
    {
        public HorarioService(IRepository<Horario> repository) : base(repository)
        {
        }

        public IEnumerable<HorarioDto> GetHorarioDtosByMateriaId(int materiaId)
        {
            return GetDto().Where(t => t.IdMateria == materiaId);
        }

        public void InscribirAlumno(HorarioDto horario)
        {
            var dto = Find(horario.Id);

            if (!horario.Estado)
            {
                if (!(dto.Proximo < dto.Cantidad))
                    throw new ValidationException($"Está sección está completa");
                dto.Proximo++;
            }    
            else
                dto.Proximo--;

            Update(dto.Id, dto);
        }

        
    }
}
