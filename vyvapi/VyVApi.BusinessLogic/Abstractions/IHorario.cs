﻿using System;
using System.Collections.Generic;
using System.Text;
using VyVApi.BusinessLogic.Dtos;

namespace VyVApi.BusinessLogic.Abstractions
{
    public interface IHorario
    {
        IEnumerable<HorarioDto> GetHorarioDtosByMateriaId(int materiaId);
        void InscribirAlumno(HorarioDto horario);
    }
}
