﻿using System;
using System.Collections.Generic;
using System.Text;
using VyVApi.BusinessLogic.Dtos;

namespace VyVApi.BusinessLogic.Abstractions
{
    public interface IMateria
    {
        IEnumerable<MateriaDto> GetMateriaDtos();
    }
}
