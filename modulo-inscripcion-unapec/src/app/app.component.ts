import { Component, OnInit } from '@angular/core';
import { IHorario } from './horarios/horario';
import { IMateria } from './materias/materia';
import { HorariosService } from './horarios/horarios.service';
import { MateriasService } from './materias/materias.service';
import { MateriasComponent } from './materias/materias.component';
import { HorariosComponent } from './horarios/horarios.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'matriculacion';

  public materias: IMateria[];
  public horarios: IHorario[];
  public textoMateria: string;
  public cuatrimestres: IMateria[];

  public horariosSeleccionado: any[] = [];



  constructor(private materiasService: MateriasService, private horariosService: HorariosService,
    private toastr: ToastrService) { }
  ngOnInit() {
    this.cargarMaterias();
  }

  cargarMaterias() {
    this.materiasService.getMaterias()
      .subscribe(materiasWS => {
        this.toastr.success('Materias Cargadas Correctamente')
        this.materias = materiasWS

        this.materias.forEach(element => {
          element.estado = false;

        });
      },
        error => this.toastr.error(`'Aqui hubo un error:: ${error}`, 'Error'));
  }

  pedirHorario(materia) {


    materia.seleccionado = true;
    // this.materias.filter(x => x.id !== materia.id).forEach(element => {
    //   element.seleccionado = false;
    // });

    this.horariosService.getHorarios(materia.id)
      .subscribe(horarioWS => {
        this.textoMateria = materia.codigo + ' - ' + materia.descripcion;
        this.horarios = horarioWS

        console.log('Me llegaron estos horarios: ')
        console.log(this.horarios)

        this.horarios.forEach(element => {
          element.seleccionado = true;
        });
        this.seleccionarx();
        // this.horarios =  this.horariosSeleccionado.filter( t => )
        // this.horariosSeleccionado = []

      },

        error =>  this.toastr.error(`'Aqui hubo un error:: ${error.error}`));

  }

  seleccionarx() {
    const xd = this.horarios.map(t => t.id);
    const xdd = this.horariosSeleccionado.map(t => t.id) as number[];
    xd.forEach(x => {
      if (xdd.includes(x)) {
        this.horarios.find(t => t.id === x).estado = false;
        this.disableHorarios(x);
      }
    });
  }

  disableHorarios(id: number) {
    this.horarios.filter(x => x.id !== id).forEach(element => {
      element.seleccionado = !element.seleccionado;
    });
  }

  inscribir(horario) {

    horario.estado = !horario.estado;
    this.horarios.filter(x => x.id !== horario.id).forEach(element => {
      element.seleccionado = !element.seleccionado;
    });

    this.materias.filter(x => x.id === horario.idMateria).forEach(element => {
      element.estado = horario.estado;
    });


    this.horariosService.setHorario(horario)
      .subscribe(res => {
        console.log('Respuesta: ', res)

        if (horario.estado) {
          this.horariosSeleccionado = this.horariosSeleccionado.filter(t => t.id !== horario.id);
        } else {
          this.horariosSeleccionado.push(horario);
          // tslint:disable-next-line:max-line-length
          this.horariosSeleccionado.find(t => t.id === horario.id).materia = this.materias.find(t => t.id === horario.idMateria).descripcion;
        }

        this.toastr.success('Horario seleccionado', 'Success')
        console.log('Horario seleccionado: ', this.horariosSeleccionado)
      },
        error => {
          this.toastr.error(`'Aqui hubo un error: ${error.error}`, 'Error')


          this.horarios.forEach(e => {
            e.seleccionado = true; e.estado = true;
          })
        });

  }

  mostrar(mate) {
    console.log(mate);
  }
  descripcionCuatrimestre(idCuatrimestre) {
    return 'CUATRIMESTRE ' + idCuatrimestre;
  }


}
