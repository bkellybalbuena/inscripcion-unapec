
import { Observable, of } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { IMateria } from './materia';
@Injectable({
  providedIn: 'root'
})
export class MateriasService {

  private apiURL: string
  private baseUrl = "http://localhost:5000/"
  constructor(private http: HttpClient) {
    this.apiURL = this.baseUrl + 'api/materias';
  }

  getMaterias(): Observable<IMateria[]> {
    return this.http.get<IMateria[]>(this.apiURL);
  }

  setMateria(materia: IMateria): Observable<IMateria> {
    return this.http.post<IMateria>(this.apiURL, materia);
  }

  deleteMateria(materiaId: string): Observable<IMateria> {
    return this.http.delete<IMateria>(this.apiURL + '/' + materiaId);
  }


}
