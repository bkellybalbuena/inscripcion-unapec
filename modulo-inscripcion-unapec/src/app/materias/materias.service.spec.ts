import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { MateriasService } from './materias.service';

describe('MateriasService', () => {
  let injector: TestBed;
  let service: MateriasService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MateriasService]
    });
    injector = getTestBed();
    service = injector.get(MateriasService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should has materias', () => {
    service.getMaterias().subscribe(desp => {
      expect(desp.length).toBeGreaterThan(0)
    })
  })
});
