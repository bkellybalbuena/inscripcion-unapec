export interface IMateria {
    id: number,
    idCuatrimestre: number,
    estado: boolean,
    codigo: string,
    descripcion: string,
    creditos: number,
    seleccionado: boolean
}
