USE [master]
GO
/****** Object:  Database [Tarea]    Script Date: 7/21/2019 1:13:09 PM ******/
CREATE DATABASE [VyV]
GO

USE [VyV]
GO


/****** Object:  Table [dbo].[Horario]    Script Date: 7/21/2019 1:13:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Horario](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdMateria] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[Aula] [varchar](100) NOT NULL,
	[Grupo] [varchar](100) NOT NULL,
	[Lun] [varchar](100) NULL,
	[Mar] [varchar](100) NULL,
	[Mir] [varchar](100) NULL,
	[Jue] [varchar](100) NULL,
	[Vie] [varchar](100) NULL,
	[Sab] [varchar](100) NULL,
	[Dom] [varchar](100) NULL,
	[Modulo] [int] NULL,
	[Seleccionado] [bit] NOT NULL,
	[Cantidad] [int] NULL,
	[Proximo] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[Materia]    Script Date: 7/21/2019 1:13:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Materia](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCuatrimestre] [int] NULL,
	[Estado] [bit] NULL,
	[Codigo] [varchar](200) NOT NULL,
	[Descripcion] [varchar](200) NOT NULL,
	[Creditos] [int] NOT NULL,
	[Seleccionado] [bit] NULL,
 CONSTRAINT [PK__Materia__3214EC07DF6B42D4] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TipoCliente]    Script Date: 7/21/2019 1:13:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

SET IDENTITY_INSERT [dbo].[Horario] ON 
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (1, 1, 1, N'0307', N'41031', N'', N'', N'', N'08:00 PM - 10:00 PM', N'', N'', N'08:00 PM - 10:00 PM', 1, 0, 10, 10)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (3, 1, 1, N'0538', N'43033', N'08:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 2)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (4, 2, 1, N'0321', N'43033', N'08:00 - - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (5, 2, 1, N'0342', N'43435', N'09:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (6, 3, 1, N'0435', N'43354', N'01:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (7, 3, 1, N'0634', N'43234', N'02:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (8, 4, 1, N'0654', N'46345', N'03:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (9, 4, 1, N'0765', N'64345', N'04:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (10, 5, 1, N'0756', N'63565', N'05:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (11, 5, 1, N'0763', N'76735', N'06:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (12, 6, 1, N'0754', N'73465', N'07:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (13, 6, 1, N'0654', N'37443', N'08:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (14, 7, 1, N'0556', N'63456', N'09:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (15, 7, 1, N'0667', N'76443', N'10:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (16, 8, 1, N'0777', N'76546', N'11:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (17, 8, 1, N'0588', N'76556', N'12:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (18, 9, 1, N'0533', N'87686', N'13:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (19, 9, 1, N'0532', N'68786', N'14:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (20, 10, 1, N'0531', N'87688', N'15:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
INSERT [dbo].[Horario] ([Id], [IdMateria], [Estado], [Aula], [Grupo], [Lun], [Mar], [Mir], [Jue], [Vie], [Sab], [Dom], [Modulo], [Seleccionado], [Cantidad], [Proximo]) VALUES (21, 10, 1, N'05323', N'86786', N'16:00 AM - 10:00 AM', NULL, N'08:00 AM - 10:00 AM', NULL, NULL, NULL, NULL, 1, 0, 10, 1)
GO
SET IDENTITY_INSERT [dbo].[Horario] OFF
GO
SET IDENTITY_INSERT [dbo].[Materia] ON 
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (1, 1, 1, N'ESP101', N'ANALISIS DE TEXTOS DISCURSIVOS', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (2, 1, 1, N'ISO100', N'FUNDAMENTOS DE INFORMATICA Y ALGORITMOS', 5, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (3, 1, 1, N'ISO105', N'FUNDAMENTOS DE SISTEMAS OPERATIVOS Y COMUNICACIONES', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (4, 1, 1, N'MAT126', N'MATEMATICA BASICA PARA INGENIERIA', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (5, 1, 1, N'SOC011', N'HISTORIA SOCIAL DOMINICANA', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (6, 1, 1, N'SOC030', N'ORIENTACION UNIVERSITARIA', 2, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (7, 1, 1, N'TEC098', N'INTRODUCCION A LA INGENIERIA', 2, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (8, 1, 1, N'ENG001', N'INGLES I', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (9, 2, 1, N'ESP102', N'REDACCION DE TEXTOS DISCURSIVOS I', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (10, 2, 1, N'ING701', N'FISICA I Y LABORATORIO', 5, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (11, 2, 1, N'ISO200', N'PROGRAMACION Y ESTRUCTURAS DE DATOS', 5, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (12, 2, 1, N'MAT127', N'MATEMATICA SUPERIOR PARA INGENIERIA', 5, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (13, 2, 1, N'SOC043', N'GESTION AMBIENTAL', 2, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (14, 2, 1, N'ENG002', N'INGLES II', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (15, 3, 1, N'ESP103', N'REDACCION DE TEXTOS DISCURSIVOS II', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (16, 3, 1, N'INF111', N'PROGRAMACION ORIENTADA A OBJETOS', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (17, 3, 1, N'ISO300', N'FUNDAMENTOS DE INGENIERIA DE SOFTWARE', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (18, 3, 1, N'ISO305', N'DISEÃ‘O WEB I', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (19, 3, 1, N'MAT222', N'ALGEBRA LINEAL', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (20, 3, 1, N'SOC150', N'RELACIONES HUMANAS', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (21, 3, 1, N'ENG003', N'INGLES III', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (22, 4, 1, N'INF165', N'BASE DE DATOS II', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (23, 4, 1, N'INF244', N'PROGRAMACION WEB I', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (24, 4, 1, N'ING702', N'FISICA II Y LABORATORIO', 5, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (25, 4, 1, N'ISO500', N'INGENIERIA DE REQUISITOS', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (26, 4, 1, N'ISO505', N'INGENIERIA DE LA USABILIDAD', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (27, 4, 1, N'MAT132', N'CALCULO Y GEOMETRIA ANALITICA II', 5, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (28, 4, 1, N'ENG005', N'INGLES V', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (29, 5, 1, N'ISO600', N'ADMINISTRACION DE CONFIGURACION', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (30, 5, 1, N'ISO605', N'DESARROLLO DE SOFTWARE CON TECNOLOGIA PROPIETARIA I', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (31, 5, 1, N'ISO610', N'DESARROLLO DE SOFTWARE CON TECNOLOGIA OPEN SOURCE I', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (32, 5, 1, N'MAT151', N'MATEMATICA DISCRETA', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (33, 5, 1, N'MAT252', N'PROBABILIDAD Y ESTADISTICA', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (34, 5, 1, N'SOC031', N'ETICA PROFESIONAL', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (35, 5, 1, N'ENG006', N'INGLES VI', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (36, 6, 1, N'ADM091', N'ADMINISTRACION I', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (37, 6, 1, N'ISO700', N'GESTION DE SITIOS WEB', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (38, 6, 1, N'ISO705', N'PROYECTO DE SOFTWARE I', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (39, 6, 1, N'ISO710', N'DESARROLLO DE SOFTWARE CON TECNOLOGIA PROPIETARIA II', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (40, 6, 1, N'ISO715', N'DESARROLLO DE SOFTWARE CON TECNOLOGIA OPEN SOURCE II', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (41, 6, 1, N'SOC250', N'METODOLOGIA DE LA INVESTIGACION CIENTIFICA', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (42, 6, 1, N'ENG007', N'INGLES VII', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (43, 7, 1, N'ADM150', N'GERENCIA DE PROCESOS', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (44, 7, 1, N'ISO915', N'FUNDAMENTOS DE SEGURIDAD DE SOFTWARE', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (45, 7, 1, N'ISO800', N'GESTION DE CALIDAD DE SOFTWARE', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (46, 7, 1, N'ISO805', N'GESTION DE PROYECTOS DE SOFTWARE', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (47, 7, 1, N'ISO810', N'INTEGRACION DE APLICACIONES CON TECNOLOGIA PROPIETARIA', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (48, 7, 1, N'ISO815', N'INTEGRACION DE APLICACIONES CON TECNOLOGIA OPEN SOURCE', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (49, 7, 1, N'ENG008', N'INGLES VIII', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (50, 8, 1, N'ADM120', N'LIDERAZGO Y TECNICAS DE SUPERVISION', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (51, 8, 1, N'IDI045', N'INGLES PARA INFORMATICA I', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (52, 8, 1, N'IND423', N'INGENIERIA ECONOMICA', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (53, 8, 1, N'ISO900', N'ADMINISTRACION DE DESARROLLO', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (54, 8, 1, N'ISO905', N'ARQUITECTURA DE DESARROLLO CON TECNOLOGIA PROPIETARIA', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (55, 8, 1, N'ISO910', N'ARQUITECTURA DE DESARROLLO CON TECNOLOGIA OPEN SOURCE', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (56, 8, 1, N'INF333', N'ADMINISTRACION DE LA SEGURIDAD DE LA INFORMACION', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (57, 9, 1, N'ADM535', N'ACTITUD EMPRENDEDORA', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (58, 9, 1, N'IDI046', N'INGLES PARA INFORMATICA II', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (59, 9, 1, N'ISO920', N'ESTANDARES DE LA INDUSTRIA DEL SOFTWARE', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (60, 9, 1, N'ISO923', N'IMPLEMENTACION DE E-COMMERCE CON TECNOLOGIA PROPIETARIA', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (61, 9, 1, N'ISO926', N'IMPLEMENTACION DE E-COMMERCE CON TECNOLOGIA OPEN SOURCE', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (62, 10, 1, N'ISO929', N'ESTRATEGIAS DE COMERCIO ELECTRONICO', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (63, 10, 1, N'ISO931', N'INTELIGENCIA DE NEGOCIOS', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (64, 10, 1, N'INF335', N'AUDITORIA INFORMATICA', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (65, 10, 1, N'ISO934', N'PROYECTO DE SOFTWARE II', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (66, 11, 1, N'ISO937', N'COMPUTACION UBICUA', 4, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (67, 11, 1, N'ISO939', N'INTEGRACION DE APLICACIONES CORPORATIVAS', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (68, 11, 1, N'SOC281', N'SEMINARIO DE GRADO', 3, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (69, 12, 1, N'OPTDEP', N'OPTATIVA DEPORTE', 0, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (70, 12, 1, N'PAS261', N'PASANTIA EMPRESARIAL', 0, 0)
GO
INSERT [dbo].[Materia] ([Id], [IdCuatrimestre], [Estado], [Codigo], [Descripcion], [Creditos], [Seleccionado]) VALUES (71, 12, 1, N'TES-MON', N'TRABAJO DE GRADO O CURSO MONOGRAFICO', 6, 0)
GO
SET IDENTITY_INSERT [dbo].[Materia] OFF


ALTER DATABASE [VyV] SET  READ_WRITE 
GO
